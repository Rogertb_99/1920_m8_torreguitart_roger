//Exemple de títol
= Pràctica: Servidor DHCP
//Taula de contingut
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
 

//Exemple de llista numerada amb espai per la resposta
.Entregar :   Captura del contingut del fitxer /etc/default/isc-dhcp-server.
//Exemple de subtítol

== Practica 1 
====
Modifiquem el fitxer principal de configuració del
servidor DHCP amb les opcions que volem 

image::dchpc1.1.jpg[]
==== 
*Modifiquem el fitxer principal de configuració del
servidor DHCP amb les opcions que volem.*
====
Captura del registre del sistema mostrant els missatges que
llença el servidor DHCP.

image::dchpc1.2.jpg[]
====

====
Resultat de la instrucció

image::dchpc1.3.jpg[]
====
Inseriu una captura del registre del sistema mostrant els missatges que
llença el servidor DHCP. 

*sudo cat /var/log/syslog | grep dhcpd*
====
El significat dels missatges de la captura 

image::dchpc1.4.jpg[]
====
==== Apartat 2:Servidor DHCP a la mateixa xarxa
====
Arxiu de configuració complet, esborreu tots els comentaris

image::dchpc1.5.jpg[]
====
==== Apartat 3:Configuració de la màquina client PC1 (a la mateixa xarxa)
====
Mostrar configuració de xarxa de PC1

image::dchpc1.6.jpg[]
====
====
Comanda per demanar una nova IP amb dhclient

image::dchpc1.7.jpg[]
====
====
Captura de les últimes línies del syslog de Farnsworth.

image::dchpc1.8.jpg[]
====
====
Captura de pantalla amb la sortida del tcpdump.

image::dchpc1.9.jpg[]
====
====
Validar l’adreça ip

image::dchpc1.10.jpg[]
====
====
Validar la porta d’enllaç

image::dchpc1.11.jpg[]
====
====
Validar servidors dns

image::dchpc1.12.jpg[]
====
====
Captura de la instrucció

image::dchpc1.13.jpg[]
====
====
Captura de la instrucció i el resultat

image::dchpc1.4.jpg[]
====
== Practica 2 Pràctica servidor DHCP

==== Apartat  4: Reserva d’adreces IP 

====
Captura de la configuració

image::dchpc2.1.jpg[]
====
====
Captura de la instrucció per validar l’adreça IP i del seu resultat.

image::dchpc2.2.jpg[]
====
==== Apartat 5: Grups d’adreces i clients registrats 

====
Captura de les modificacions de l’arxiu de configuració.

image::dchpc2.3.jpg[]
====
====
Captura del terminal amb la renovació i la IP adquirida.

image::dchpc2.4.jpg[]
====
====
Captura del terminal amb la renovació i la IP adquirida.

image::dchpc2.5.jpg[]
====
== Practica 3 Pràctica servidor DHCP

==== Apartat 6: Configuració de PC1 i PC2 a una xarxa diferent
====
Captura de la nova configuració.

image::dhcp3.2.jpg[]
====
====
Comprovació de la IP de PC1 i PC2.

image::dhcp3.3.jpg[]
====
==== Apartat 7:Configuració d’un servidor secundari
====
Instal·lació del NTP a Farnsworth i a Hermes.

image::dhcp3.4.jpg[]
====
====
Canvis a la configuració de Farnsworth.

image::dhcp3.5.jpg[]
====
====
Demostració que el servidor DHCP s’està executant a Hermes. 

image::dhcp3.6.jpg[]
====
====
Captura de la configuració de failover a Farnsworth. 

image::dhcp3.7.jpg[]
====
====
 Captura de la configuració de failover a Hermes.

image::dhcp3.8.jpg[]
====
====
Nova configuració del DHCP relay.

image::dhcp3.9.jpg[]
====
====
Demostració que el failover funciona.

image::dhcp3.10.jpg[]
====