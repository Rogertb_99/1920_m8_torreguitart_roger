//Exemple de títol
= Pràctica servidor HTTP
//Taula de contingut
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

//Exemple de subtítol
== Practica 1

=== Apartat 1: Instal·lació de l’Apache a Hermes


Instal·la el paquet apache2 a Hermes i comprova que ja s’hi pot accedir
des de la xarxa local.
====
Example 1. Entregar
Instal·lació de l’apache2

image::fots 1/http1.1.jpg[]
====
====
Example 2. Entregar
Comprovació de connexió des de la xarxa local

image::fots 1/http1.2.jpg[]
====



Verifica quins ports ha obert la instal·lació de l’Apache des del servidor. Per tal de fer-ho pots utilitzar la comanda netstat.

====
Example 3. Entregar
Ports oberts amb la instal·lació d’Apache2

image::fots 1/http1.3.jpg[]
====


Verifica l’estat d’execució d’Apache amb systemctl status... Una vegada fet mostra els logs inicials d’Apache al syslog

====
Example 4. Entregar
Estat d’execució

image::fots 1/http1.4.jpg[]
====
====
Example 5. Entregar
Logs inicials d’Apache2

image::fots 1/http1.5.jpg[]
====

==== Apartat 2: Creació de hosts virtuals

En aquest apartat farem que el nostre servidor respongui a dos noms de host
diferents. Per una banda, tindrem el nom per defecte, *nom.cognom.lan*, el
mateix que vam utilitzar a la pràctica dns.

A aquest afegirem el host
*cognom.nom.lan*, que es dirigirà al mateix servidor, però a una pàgina web
diferent.

====
Example 6. Entregar
Captura del fitxer /etc/bind/named.conf.local.

image::fots 1/http1.6.jpg[]
====

====
Example 7. Entregar
Captura del fitxer de la zona.

image::fots 1/http1.7.jpg[]
====
====
Example 8. Entregar
Comprovació que la nova zona funciona correctament.

image::fots 1/http1.8.jpg[]
====

Anem a crear ara les pàgines web de cada domini.
====
Example 9. Entregar
Sortida de ls -R on es vegi l’estructura de fitxers i directoris de /var/www.

image::fots 1/http1.9.jpg[]
====
====
Example 10. Entregar
Cadascun dels dos fitxers index.html.

image::fots 1/http1.10.jpg[]
====
====
Example 11. Entregar
Cadascun dels dos fitxers de configuració creats.

image::fots 1/http1.11.jpg[]
====
====
Example 12. Entregar
Instruccions utilitzades per activar i desactivar els hosts virtuals.

image::fots 1/http1.12.jpg[]
====
====
Example 13. Entregar
Sortida de l’ordre ls on es vegin els sites actius a sites-enabled.

image::fots 1/http1.13.jpg[]
====
====
Example 14. Entregar
Captura del navegador connectat a cadascun dels dos sites.

image::fots 1/http1.14.jpg[]
====

====Apartat 3: Configuració del primer host virtual

Les següents configuracions s’han d’aplicar al site nom.cognom.lan.
====
Example 15. Entregar
Canvis fets a la configuració.

image::fots 1/http1.15.jpg[]
====


Per tal que això funciona, el servidor DNS ha de resoldre correctament l’adreça
www.nom.cognom.lan a la teva adreça IP.
====
Example 16. Entregar
Canvis fets a la configuració.

image::fots 1/http1.16.jpg[]
====
====
Example 17. Entregar
Comprovació que funciona.

image::fots 1/http1.17.jpg[]
====
====
Example 18. Entregar
Canvis fets a la configuració.

image::fots 1/http1.18.jpg[]
====
====
Example 19. Entregar
Comprovació que funciona.

image::fots 1/http1.19.jpg[]
====
====
Example 20. Entregar
Canvis fets a la configuració.

image::fots 1/http1.20.jpg[]
====
====
Example 21. Entregar
Canvis fets a la configuració.

image::fots 1/http1.21.jpg[]
====
====
Example 22. Entregar
Comprovació que funciona.

image::fots 1/http1.22.jpg[]

image::fots 1/http1.23.jpg[]
====

== Practica 2

=== Apartat 4: Configuració del segon host virtual
====
Example 1. Entregar
Canvis fets a la configuració.

image::fots 2/http2.1.jpg[]
====
Un cop modificada la configuració, activa el nou site.



====
Example 2. Entregar
Instrucció utilitzada.

image::fots 2/http2.2.jpg[]

====
Comprova amb el navegador que podem accedir tant per HTTP com per HTTPS a
aquest site.


====
Example 3. Entregar
Comprovació que funciona.

image::fots 2/http2.3.jpg[]
====
Comprova amb el navegador que si accedim ara per HTTP, la petició es
modifica automàticament.
====
Example 4. Entregar
Captura del fitxer de registre d’accés de l’Apache, on es veu la reescriptura
de l’adreça.

image::fots 2/http2.4.jpg[]
====

=== Apartat 5: Instal·lació de PHP

Ara que tenim els hosts virtuals funcionant, procedirem a instal·lar els mòduls necessaris per a què el nostre servidor suporti projectes realitzats en PHP.
====
Example 5. Entregar
Captura del resultat de la comanda

image::fots 2/http2.5.jpg[]

image::fots 2/http2.6.jpg[]

====

Obre ara un navegador i accedeix al script emmagatzemat al primer host virtual.

====
Example 6. Entregar
Captura del resultat de còrrer l’script.

image::fots 2/http2.9.jpg[]


image::fots 2/http2.10.jpg[]
====

== Practica 3

=== Apartat 6: Instal·lació de MySql

Ara procedirem a instal·lar Mysql a Hermes per completar el nostre LAMP, per fer-ho, executa el següent
====
Example 1. Entregar
Sortida de la comanda systemctl.

image::fots 3/http3.1.jpg[]

image::fots 3/http3.2.jpg[]
====

==== 6.1 Creació d’un password per a l’usuari administratiu (root) per Mysql
====
Example 2. Entregar
Sortida de l’script mysql_secure_installation.

image::fots 3/http3.3.jpg[]

image::fots 3/http3.4.jpg[]
====

====  6.2 Canviar el mètode d’autenticació per a l’usuari root de Mysql
====
Example 3. Entregar
Sortida de la comanda anterior.

image::fots 3/http3.5.jpg[]

image::fots 3/http3.6.jpg[]
====
Ara comproveu si el connector d’autenticació actual s’ha canviat o no mitjançant l’ordre:

mysql> SELECT user,authentication_string,plugin,host FROM mysql.user;
====
Example 4. Entregar
Sortida de la comanda anterior.

image::fots 3/http3.7.jpg[]
====

=== Apartat 7: Instal·lació de PhpMyadmin
====
Example 5. Entregar
Procés d’instal·lació.

image::fots 3/http3.8.jpg[]
====
 
==== 7.1 Crear un usuari dedicat per accedir al panell de phpMyAdmin

Finalment sortiu de la shell de mysql.

====
Example 6. Entregar
Sortida de les anteriors comandes.

image::fots 3/http3.9.jpg[]
====
====
Example 7. Entregar
Vista del panell de control de phpMyAdmin amb el nou usuari creat

image::fots 3/http3.10.jpg[]

image::fots 3/http3.11.jpg[]


image::fots 3/http3.12.jpg[]
====





















