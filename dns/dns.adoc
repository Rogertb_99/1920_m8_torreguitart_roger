//Exemple de títol
= Pràctica: servidor Dns
//Taula de contingut
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

//Exemple de subtítol
== Practica 1

=== Apartat 1: Instal·lació del servidor DNS a Zoidberg

El primer que farem serà comprovar la configuració DNS actual de Zoidberg.
Prova a consultar l’adreça IP del nom de domini louvre.fr. Per fer a mà una
consulta DNS podem utilitzar les comandes dig o nslookup.

====
Example 1. Entregar
Captura del resultat de la consulta DNS.

image::foto/DNS1.1.jpg[]
====
Instal·lem ara el servidor DNS a Zoidberg

====
Example 2. Entregar
Captura instrucció que instal·la el servidor DNS.

image::foto/DNS1.2.jpg[]
====
Comprovem que el servidor DNS s’està executant. Utilitza netstat per veure
per quin port està escoltant.
====
Example 3. Entregar
Captura del resultat de la instrucció netstat que mostra el servidor DNS
funcionant.

image::foto/DNS1.3.jpg[]

====
Modificarem  el fitxer netplan i escriu-hi l’adreça de Zoidberg (servidor DNS).


*Importan fer [underline]#netplan apply# per a què els canvis es produeixin.*

====
Example 4. Entregar
Captura de la execució de systemd-resolve --status .

image::foto/DNS1.3.2.jpg[]
====
Provem el nou servei RNDC 

====
Example 5. Entregar
Captura de la instrucció utilitzada.

image::foto/DNS1.4.jpg[]
====

Per fer que el servidor DNS només treballi amb IPv4 modificarem el fitxer
/etc/default/named i canviarem la línia OPTIONS de OPTIONS="-u bind"  a
OPTIONS="-4 -u bind".

====
Example 6. Entregar
Captura de /etc/default/named.

image::foto/DNS1.5.jpg[]
====


Com sempre,cal reiniciar el servidor DNS cada cop que en modifiquem
la configuració per tal que s’adapti a la noves opcions.
====
Example 7. Entregar
Captura de /etc/bind/named.conf.options.

image::foto/DNS1.6.jpg[]
====


Consulta la IP d’un nou domini
====
Example 8. Entregar
Captura de la instrucció utilitzada i el seu resultat.

image::foto/DNS1.7.jpg[]
====
Comprova la informació al registre del
bind.
====
Example 9. Entregar
Part del registre del bind corresponent a l’última petició.

image::foto/DNS1.8.jpg[]
====



Utilitzant rndc ,comprova quina informació s’ha
guardat sobre el domini


====

Example 10. Entregar
Instrucció per bolcar el cau a un fitxer i contingut obtingut.

image::foto/DNS1.10.jpg[]
====

Desactiva ara la depuració del servidor (rndc).

====

Example 11. Entregar
Instrucció per desactivar la depuració del servidor DNS.

image::foto/DNS1.11.jpg[]
====

//Exemple de subtítol
== Practica 2

==== Apartat 2: Configuració d’un servidor DNS només cache


En aquest apartat configurarem el servidor DNS
====
Example 1. Entregar
Captura de les modificacions fetes a la configuració.

image::dns 2/DNS2.1.jpg[]
====

==== Apartat 3: Configuració d’un servidor DNS forwarding


====
Example 2. Entregar
Captura de les modificacions fetes a la configuració.

image::dns 2/DNS2.1.2.1.jpg[]

image::dns 2/DNS2.1.2.2.jpg[]

image::dns 2/DNS2.1.2.3.jpg[]

image::dns 2/DNS2.1.2.4.jpg[]

====
Comprova que la configuració està ben escrita utilitzant named-checkconf.


Reinicia el servidor i comprova que els canvis funcionen activant i
consultant la depuració.

====
Example 3. Entregar
Sentències utilitzades i resultats

image::dns 2/DNS2.1.2.5.jpg[]

image::dns 2/DNS2.1.2.6.jpg[]
====

== Practica 3

Apartat 4: Configuració dels clients

Ja tenim el servidor Zoidberg funcionant com a servidor DNS.


Configura els clients per a què cerquin per defecte al teu
domini, si ho fas a través d’un servidor DHCP recorda afegir-ho.
És a dir, que si no especifiquen el FQDN, es consideri que els hosts són d’aquest domini.

====
Example 1. Entregar
Comprovació a Zoidberg i a PC1.

image::fotos dns 3/DNS3.1.jpg[]

image::fotos dns 3/DNS3.2.jpg[]

image::fotos dns 3/DNS3.3.jpg[]
====

Apartat 5: Creació de la zona interna

La configuració d’una nova zona
====
Example 2. Entregar
Fixer /etc/bind/named.conf.local.

image::fotos dns 3/DNS3.4.jpg[]
====

Apartat 6: Configuració de la resolució directa

Crea el fitxer de resolució directa per al teu domini. 
====
Example 3. Entregar
Configuració de la resolució directa.

image::fotos dns 3/DNS3.5.jpg[]

image::fotos dns 3/DNS3.6.jpg[]
====

Apartat 7: Configuració de la resolució inversa

Crea el/els fitxers de resolució inversa per al teu domini.
====
Example 4. Entregar
Configuració de la resolució inversa.

image::fotos dns 3/DNS3.7.jpg[]

image::fotos dns 3/DNS3.8.jpg[]

image::fotos dns 3/DNS3.9.jpg[]

image::fotos dns 3/DNS3.10.jpg[]

image::fotos dns 3/DNS3.11.jpg[]

image::fotos dns 3/DNS3.12.jpg[]

image::fotos dns 3/DNS3.13.jpg[]
====

Apartat 8: Comprovació de la configuració

Comprovacions des del client PC1 el funcionament del servidor DNS.
====
Example 5. Entregar
Comprovacions des de PC1.

image::fotos dns 3/DNS3.14.jpg[]

image::fotos dns 3/DNS3.15.jpg[]
====





























