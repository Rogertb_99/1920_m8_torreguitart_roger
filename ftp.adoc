//Exemple de títol
= Pràctica:  servidor FTP
//Taula de contingut
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

//Exemple de subtítol
== Practica 1

=== Apartat 1: Instal·lador del servidor FTP a Fry

====
Example 1. Entregar
Captura de la transacció FTP entre el client i Fry

image::FTP1.1.jpg[]
====
=== Apartat 2:Configuració de l’accés anònim

Crearem el directori ftpusers dins de /home. I dins de ftpusers crearem
un directori per cada usuari del servei FTP

====
Example 2. Entregar
Captura de la sortida de ls -ld sobre el nou directori.

image::FTP1.2.jpg[]
====

Ara activem l’accés anònim editant el fitxer de configuració
/etc/vsftpd.conf. Cal configurar les opcions anonymous_enable i
anon_root.

image::FTP1.3.jpg[]
====
Example 3. Entregar
Captura de les modificacions a la configuració.

image::FTP1.4.jpg[]
====
Un cop activat l’accés anònim, crea un fitxer dins del seu home. Comprova
que el fitxer es pot descarregar accedint com usuari anònim des de l’ordinador
físic.

====
Example 4. Entregar
Captura d’una transacció utilitzant l’accés anònim

image::FTP1.5.jpg[]

====
=== Apartat 3:Separació de la configuració del FTP en xarxes
====
Example 5. Entregar
Canvis al fitxer de configuració

image::FTP1.6.jpg[]
====
====
Example 6. Entregar
Fitxer /etc/hosts.allow

image::FTP1.7.jpg[]
====

====

Example 7. Entregar
Comprovació accés exterior

image::FTP1.8.jpg[]
====

====
Example 8. Entregar
Comprovació accés intern

image::FTP1.9.jpg[]
====

== Practica 2

=== Apartat 4: Configuració de l’espai compartit

Ara crearem un usuari només per accés al FTP. Utilitzarem l’ordre useradd amb
una colla de modificadors.

====
Example 1. Entregar
Ordre utilitzada per crear l’usuari ftpuser

image::FTP2.1.jpg[]
====

A continuació li hen d’assignar una contrasenya a l’usuari.

====
Example 2. Entregar

Ordre utilitzada per donar una contrasenya a l’usuari ftpuser

image::FTP2.2.jpg[]
====



Podem comprovar que l’usuari ftpuser no pot connectar-se directament al
sistema. En canvi, sí que pot connectar a través de FTP.
====
Example 3. Entregar
Comprovació que l’usuari ftpuser pot connectar-se al servei FTP.

image::FTP2.3.jpg[]
====

Cal reiniciar el servei per tal que els canvis tinguin efecte.

====
Example 4. Entregar
Instrucció per reiniciar el servei vsftpd.

image::FTP2.4.jpg[]
====
====
Example 5. Entregar
Instrucció per treure el permís d’escriptura al home de l’usuari ftpuser.

image::FTP2.5.jpg[]
====


Després de reiniciar el servei, comprova que només aquests dos usuaris poden
connectar-se, però que es denega l’accés a qualsevol altre usuari del sistema.

====
Example 6. Entregar
Comprovació que ftpuser i anonymous poden connectar, però que el teu usuari
habitual no.

image::FTP2.6.jpg[]
====

==== Apartat 5: Configuració de l’espai per a cada usuari

====
Example 7. Entregar
Canvis fets a /etc/vsftpd.lan.conf.

image::FTP2.7.jpg[]
====


Comprova que amb aquest canvi un usuari pot, a més de baixar fitxers del
servidor, pujar-hi els seus propis fitxers (evidentment, només allà on
tingui permís d’escriptura).

====
Example 8. Entregar
Comprovació que un usuari pot pujar fitxers al servidor.

image::FTP2.8.jpg[]
====

== Practica 3

=== Apartat 6: Configuració de la capa de xifratge SSL

====
Example 1. Entregar
Captura on es vegi el Filezilla connectat a Fry.

image::FTP3.jpg[]

Example 2. Entregar
Captura del Filezilla on es vegi alguna informació del certificat digital.

image::FTP3.jpg[]
====



