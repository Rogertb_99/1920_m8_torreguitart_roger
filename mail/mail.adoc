//Exemple de títol
= Pràctica servidor correu electrònic
//Taula de contingut
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

//Exemple de subtítol
== Practica 1

=== Apartat 1: Configuració del servidor DNS a la xarxa local
====
Example 1. Entregar
Captura del fitxer /etc/bind/named.conf.local.

image::mail1.1.jpg[]

image::mail1.2.jpg[]

====
====
Example 2. Entregar
Captura del fitxer de la zona.

image::mail1.3.jpg[]

image::mail1.4.jpg[]
====
====
Example 3. Entregar
Comprovació que la nova zona funciona correctament.

image::fots 1/mail1.5.jpg[]
====

=== Apartat 2: Preparació de la màquina Amy_wong
====
Example 4. Entregar
Captura del contingut del fitxer /etc/netplan/xxx.yaml de Amy_wong.

image::fots 1/mail1.6.jpg[]
====

=== Apartat 3: Instal·lador del servidor de correu electrònic a Amy_wong
====
Example 5. Entregar
Captura del procés d’instal·lació.

image::fots 1/mail1.7.jpg[]
====
====
Example 6. Entregar
Captura de systemctl status.

image::fots 1/mail1.8.jpg[]
====
====
Example 7. Entregar
Captura de la comanda postconf ..

image::fots 1/mail1.9.jpg[]

image::fots 1/mail1.10.jpg[]
====
=== Apartat 4: Comprovació de la configuració en local

Només amb aquesta configuració bàsica els usuaris de la màquina local ja es
poden enviar missatges.
====
Example 8. Entregar
Captura de la seqüència d’enviament i lectura del correu

image::fots 1/mail1.11.jpg[]

image::fots 1/mail1.12.jpg[]
====
=== Apartat 5: Comprovació del servidor de correu via telnet

Un altre mètode per comprovar que podem enviar un missatge de forma remota es utilitzant l’ordre telnet.

====
Example 9. Entregar
Conversa a Amy_wong utilitzant telnet.

image::fots 1/mail1.13.jpg[]

image::fots 1/mail1.14.jpg[]
====
====
Example 10. Entregar
Missatge rebut a Amy_wong.

image::fots 1/mail1.15.jpg[]

image::fots 1/mail1.16.jpg[]

image::fots 1/mail1.17.jpg[]

====

== Practica 2

=== Apartat 6: Configuració d’un certificat SSL

Per crear el certificat podem executar:
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/mail.key -out /etc/ssl/certs/mailcert.pem
====
Example 1. Entregar
Sortida de l’ordre anterior

image::fots 2/mail2.1.jpg[]
====

=== Apartat 7: Configuració del Postfix - servei submission
====
Example 2. Entregar
Captura dels canvis a la configuració.

image::fots 2/mail2.2.jpg[]
====

=== Apartat 8: Configuració del Postfix - opcions generals
====
Example 3. Entregar
Captura dels canvis a la configuració.

image::fots 2/mail2.3.jpg[]
====
====
Example 4. Entregar
Captura dels canvis a la configuració.

image::fots 2/mail2.4.jpg[]
====
====
Example 5. Entregar
Captura dels canvis a la configuració.

image::fots 2/mail2.5.jpg[]
====
====
Example 6. Entregar
Captura dels canvis a la configuració.

image::fots 2/mail2.6.jpg[]
====
====
Example 7. Entregar
Captura de netstat.

image::fots 2/mail2.7.jpg[]
====


== Practica 3

=== Apartat 9: configuració dels àlies
Després de modificar aquest fitxer cal executar l’ordre newaliases per tal
que el Postfix vegi els canvis que hi hem fet.
====
Example 1. Entregar
Captura dels canvis a la configuració.

image::fots 3/mail3.1.jpg[]
====

=== Apartat 10: Configuració del protocol IMAP

====
Example 2. Entregar
Captura dels canvis a la configuració

image::fots 3/mail3.2.jpg[]

image::fots 3/mail3.3.jpg[]
====

==== Configurar la localització dels correus

====
Example 3. Entregar
Captura dels canvis a la configuració.

image::fots 3/mail3.3.jpg[]

image::fots 3/mail3.4.jpg[]
====

==== Configurar l’autenticació

====
Example 4. Entregar
Captura dels canvis a la configuració.

image::fots 3/mail3.5.jpg[]

image::fots 3/mail3.6.jpg[]
====
==== Configurar SSL/TLS
====
Example 5. Entregar
Captura dels canvis a la configuració.

image::fots 3/mail3.7.jpg[]
====
==== Configurar SASL Auth
==== Autocrear les carpetes Sent i Trash (enviar i paperera)
====
Example 6. Entregar
Captura dels canvis a la configuració.

image::fots 3/mail3.8.jpg[]

image::fots 3/mail3.9.jpg[]

image::fots 3/mail3.10.jpg[]
====
====
Example 7. Entregar
Captura de l’estat del servei: systemctl status..

image::fots 3/mail3.11.jpg[]
====
====
Example 8. Entregar
Captura dels ports oberts pel servidor

image::fots 3/mail3.12.jpg[]
====
==== Utilitzar Dovecot per entregar els emails al magatzem de Missatges

Reinicia tant Dovecot com Postfix

====
Example 9. Entregar
Captura dels canvis a la configuració de tots dos serveisV

image::fots 3/mail3.13.jpg[]

image::fots 3/mail3.14.jpg[]

image::fots 3/mail3.15.jpg[]
====

=== Apartat 11: Configuració de Thunderbird
====
Example 10. Entregar
Captura de la configuració del Thunderbird

image::fots 3/mail3.16.jpg[]
====

====
Example 11. Entregar
Captura del procés de redacció d’un email des de Thunderbird

image::fots 3/mail3.17.jpg[]

image::fots 3/mail3.18.jpg[]
====

====
Example 12. Entregar
Captura de les bústies dels dos usuaris (amb missatges)

image::fots 3/mail3.20 .jpg[]

image::fots 3/mail3.21.jpg[]

image::fots 3/mail3.22.jpg[]

====
